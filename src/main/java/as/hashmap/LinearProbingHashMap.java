package as.hashmap;

import java.util.Objects;

public class LinearProbingHashMap<KeyType, ValueType> implements MyHashMap<KeyType, ValueType> {
    private final int ARRAY_SIZE;
    private Object[] mapEntries;

    LinearProbingHashMap(int size) {
        ARRAY_SIZE = size;
        mapEntries = new Object[ARRAY_SIZE];
    }

    @Override
    public void put(KeyType key, ValueType value) {
        Objects.requireNonNull(key);
        int keyHashCode = calculateHashCodeInArray(key);
        for (; mapEntries[keyHashCode] != null; keyHashCode = (keyHashCode + 1) % ARRAY_SIZE) {
        }
        mapEntries[keyHashCode] = new Entry(key, value);
    }

    @Override
    public ValueType get(KeyType key) {
        Objects.requireNonNull(key);
        int keyHashCode = calculateHashCodeInArray(key);
        for (; mapEntries[keyHashCode] != null; keyHashCode = (keyHashCode + 1) % ARRAY_SIZE) {
            if(((Entry)mapEntries[keyHashCode]).key.equals(key)){
                return ((Entry)mapEntries[keyHashCode]).value;
            }
        }
        return null;
    }

    private int calculateHashCodeInArray(KeyType key) {
        return Math.abs(key.hashCode()) % ARRAY_SIZE; //this is faulty
    }


    class Entry {
        KeyType key;
        ValueType value;

        Entry(KeyType key, ValueType value) {
            this.key = key;
            this.value = value;
        }
    }
}
