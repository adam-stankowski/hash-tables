package as.hashmap;

public interface MyHashMap<KeyType, ValueType> {
    void put(KeyType key, ValueType value);
    ValueType get(KeyType key);
}
