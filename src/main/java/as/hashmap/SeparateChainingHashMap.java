package as.hashmap;

import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

public class SeparateChainingHashMap<KeyType, ValueType> implements MyHashMap<KeyType, ValueType> {

    private class Entry {
        private KeyType key;
        private ValueType value;

        Entry(KeyType key, ValueType value) {
            this.key = key;
            this.value = value;
        }

        KeyType getKey() {
            return key;
        }

        ValueType getValue() {
            return value;
        }

        ValueType setValue(ValueType value) {
            this.value = value;
            return this.value;
        }
    }

    private final int ARRAY_SIZE;
    private List<Entry>[] array;

    public SeparateChainingHashMap(int arraySize) {
        this.ARRAY_SIZE = arraySize;
        array = (List<Entry>[]) new List[ARRAY_SIZE];
    }

    @Override
    public void put(KeyType key, ValueType value) {
        Objects.requireNonNull(key);

        int myHashCode = calculateHashCodeInArray(key); //this is buggy as int in -2^31 - 2^31-1

        if (array[myHashCode] == null) {
            array[myHashCode] = new ArrayList<>();
        }
        array[myHashCode].add(new Entry(key, value));
    }

    private int calculateHashCodeInArray(KeyType key) {
        return Math.abs(key.hashCode()) % ARRAY_SIZE;
    }

    @Override
    public ValueType get(KeyType key) {
        Objects.requireNonNull(key);
        int hashCode = calculateHashCodeInArray(key);

        List<Entry> entriesAtIndex = array[hashCode];
        if (entriesAtIndex == null) {
            return null;
        }

        for (Entry entry : entriesAtIndex) {
            if(key.equals(entry.getKey())){
                return entry.getValue();
            }
        }
        return null;
    }
}
