package as.hashmap;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.ExpectedException;

import static org.hamcrest.core.Is.is;

public class MyHashMapTest {
    @Rule
    public ExpectedException expectedException = ExpectedException.none();

    private MyHashMap<String, String> myHashMap;

    @Before
    public void setUp() {
        myHashMap = new LinearProbingHashMap<>(10);
    }

    @Test
    public void whenKeyNullThenException() {
        expectedException.expect(NullPointerException.class);
        myHashMap.put(null, "Any String");

        expectedException.expect(NullPointerException.class);
        myHashMap.get(null);
    }

    @Test
    public void whenAddingSingleValueThenItsTakenOut() {
        String oneKey = "OneKey";
        String oneValue = "OneValue";
        myHashMap.put(oneKey, oneValue);
        String retrieved = myHashMap.get(oneKey);
        Assert.assertThat(retrieved, is(oneValue));
    }

    @Test
    public void whenMoreThanOneItemsAddedThenTheyAreCorrectlyRetrieved() {
        String oneKey = "OneKey";
        String oneValue = "OneValue";
        String twoKey = "TwoKey";
        String twoValue = "TwoValue";

        myHashMap.put(oneKey, oneValue);
        myHashMap.put(twoKey, twoValue);
        String retrieved = myHashMap.get(oneKey);
        Assert.assertThat(retrieved, is(oneValue));

        retrieved = myHashMap.get(twoKey);
        Assert.assertThat(retrieved, is(twoValue));
    }

    @Test
    public void whenNoKeyExistsThenNull() {
        String oneKey = "OneKey";
        String oneValue = "OneValue";
        String twoKey = "TwoKey";
        String twoValue = "TwoValue";

        myHashMap.put(oneKey, oneValue);
        myHashMap.put(twoKey, twoValue);
        String retrieved = myHashMap.get("WrongKey");
        Assert.assertNull(retrieved);
    }

    @Test
    public void whenSameKeyHashCodeThenCorrectValuesRetrieved() {
        String oneKey = "Aa";
        String oneValue = "AaValue";
        String twoKey = "BB";
        String twoValue = "BBValue";

        Assert.assertEquals(oneKey.hashCode(), twoKey.hashCode());

        myHashMap.put(oneKey, oneValue);
        myHashMap.put(twoKey, twoValue);
        String retrieved = myHashMap.get(oneKey);
        Assert.assertThat(retrieved, is(oneValue));

        retrieved = myHashMap.get(twoKey);
        Assert.assertThat(retrieved, is(twoValue));
    }
}